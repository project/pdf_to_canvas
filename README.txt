CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Usage
 * Maintainers

INTRODUCTION
------------

The module provide you Manually Add multiple cron time in the second.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/pdf_to_canvas

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/pdf_to_canvas


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

 * Install the PDF To Canvas File Field Formatter module as you would normally install a contributed Drupal module. Visit https://www.drupal.org/docs/extending-drupal/installing-modules for further information.


CONFIGURATION
-------------

 * The Module have only small configuration goto to any file field manage display setting and set PDF to canvas format (Note: File field supoort must be .pdf)


USAGE
-----

 * Using PDF to canvas field Formatter
 * Render .pdf to single Canvas using pdf.js


MAINTAINERS
-----------

 * Omprakash Mankar (omrmankar)- https://www.drupal.org/u/omrmankar
