<?php

namespace Drupal\pdf_to_canvas\Plugin\Field\FieldFormatter;

use Drupal\file\Entity\File;
use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the 'pdf_to_canvas_field_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "pdf_to_canvas_field_formatter",
 *   label = @Translation("PDF To Canvas"),
 *   field_types = {
 *     "file"
 *   }
 * )
 */
class PdfToCanvasFieldFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      // Implement default settings.
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return [
      // Implement settings form.
    ] + parent::settingsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    // Implement settings summary.
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $field_name = $items->getName();
    $node = \Drupal::routeMatch()->getParameter('node');
    if (!empty($node)) {
      $nid = $node->id();
      $node = \Drupal::entityTypeManager()->getStorage('node')->load($nid);
      $tid = $node->get($field_name)->getValue();
      $fid = $tid[0]['target_id'];
      $file = File::load($fid);
    }

    if (!empty($file)) {
      $url = $file->createFileUrl();
    }

    $elements = [
      '#theme' => 'pdf_to_canvas',
    ];

    $elements['#attached']['drupalSettings']['pdf_to_canvas'] = [
      'file_url' => urldecode($url),
    ];
    return $elements;
  }

  /**
   * Generate the output appropriate for one field item.
   *
   * @param \Drupal\Core\Field\FieldItemInterface $item
   *   One field item.
   *
   * @return string
   *   The textual output generated.
   */
  protected function viewValue(FieldItemInterface $item) {
    // The text value has no text format assigned to it, so the user input
    // should equal the output, including newlines.
    return nl2br(Html::escape($item->value));
  }

}
