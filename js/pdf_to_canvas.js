/** @format */

Drupal.behaviors.pdf_to_canvas = {
  attach: function (context, settings) {
    "use strict";

    window.onload = () => {
      const controls = {
        pdf: null,
        current: 1,
        zoom: 1.2,
      };
      var pageNumPending = null;
      var pageRendering = false;

      const url = drupalSettings.pdf_to_canvas.file_url;

      // Loaded via <script> tag, create shortcut to access PDF.js exports.
      var pdfjsLib = window["pdfjs-dist/build/pdf"];

      // The workerSrc property shall be specified.
      pdfjsLib.GlobalWorkerOptions.workerSrc =
        "//cdn.jsdelivr.net/npm/pdfjs-dist@2.10.377/build/pdf.worker.js";

      pdfjsLib.getDocument(url).promise.then(function (pdfDoc_) {
        controls.pdf = pdfDoc_;
        document.getElementById("total_page").innerHTML =
          pdfDoc_._pdfInfo.numPages;
        jQuery(document, context)
          .once("pdf_to_canvas")
          .each(function () {
            queueRenderPage(controls.current);
          });
      });

      function queueRenderPage(num) {
        if (pageRendering) {
          pageNumPending = num;
        } else {
          render();
        }
      }

      function render() {
        pageRendering = true;
        controls.pdf.getPage(controls.current).then((page) => {
          const canvas = document.getElementById("pdf_render");
          const ctx = canvas.getContext("2d");
          const viewport = page.getViewport({
            scale: controls.zoom,
          });

          canvas.width = viewport.width;
          canvas.height = viewport.height;

          var renderContext = {
            canvasContext: ctx,
            viewport: viewport,
          };
          var renderTask = page.render(renderContext);
          renderTask.promise.then(function () {
            pageRendering = false;
            if (pageNumPending !== null) {
              // New page rendering is pending
              render(pageNumPending);
              pageNumPending = null;
            }
          });
        });
        const element = document.getElementById("current_page");
        element.value = controls.current;
        element.setAttribute("max", controls.pdf._pdfInfo.numPages);
      }

      document.getElementById("prev").addEventListener("click", (e) => {
        if (controls.pdf === null || controls.current === 1) return;
        controls.current = controls.current - 1;
        queueRenderPage(controls.current);
      });

      document.getElementById("next").addEventListener("click", (e) => {
        if (
          controls.pdf === null ||
          controls.current >= controls.pdf._pdfInfo.numPages
        )
          return;
        controls.current = controls.current + 1;
        queueRenderPage(controls.current);
      });

      document
        .getElementById("current_page")
        .addEventListener("change", (e) => {
          const value = Number(e.target.value);
          if (value >= 1 && value <= controls.pdf._pdfInfo.numPages) {
            controls.current = value;
            queueRenderPage(controls.current);
          }
        });

      document.getElementById("zoom_in").addEventListener("click", (e) => {
        if (controls.pdf === null) return;
        controls.zoom = controls.zoom + 0.1;
        queueRenderPage(controls.current);
      });

      document.getElementById("zoom_out").addEventListener("click", (e) => {
        if (controls.pdf === null || controls.zoom == 0.8) return;
        controls.zoom = controls.zoom - 0.1;
        queueRenderPage(controls.current);
      });

      document.getElementById("full_screen").addEventListener("click", (e) => {
        toggleFullScreen();
      });

      function toggleFullScreen() {
        if (!document.fullscreenElement) {
          document.documentElement.requestFullscreen();
        } else {
          if (document.exitFullscreen) {
            document.exitFullscreen();
          }
        }
      }
    };
  },
};
